using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class GhostManager : MonoBehaviour
{
    public float timeBetweenSamples = 0.25f;
    public GhostLapData bestLapSO;              // Scriptable object that will contain the ghost data
    public GhostLapData ActualLapSO;
    public GameObject carToRecord;
    public GameObject carToPlay;
   

    // RECORD VARIABLES
    private bool shouldRecord = false;
    private float totalRecordedTime = 0.0f;
    public float currenttimeBetweenSamples = 0.0f;

    // REPLAY VARIABLES
    public bool shouldPlay = false;
    private float totalPlayedTime = 0.0f;
    private float currenttimeBetweenPlaySamples = 0.0f;
    public int currentSampleToPlay = 0;

    // POSITIONS/ROTATIONS
    private Vector3 lastSamplePosition = Vector3.zero;
    private Quaternion lastSampleRotation = Quaternion.identity;
    private Vector3 nextPosition;
    private Quaternion nextRotation;

    //Controlamos el inicio de la grabaci�n con una bandera
    private Boolean flagRecord = false;

    private Boolean shouldRePlay = false;





    public Boolean replace = false;
 


    #region RECORD GHOST DATA
    public void StartRecording()
    {
        Debug.Log("START RECORDING");
        shouldRecord = true;
        shouldPlay = false;

        // Seteamos los valores iniciales
        totalRecordedTime = 0;
        currenttimeBetweenSamples = 0;

        // Limpiamos el scriptable object
        //bestLapSO.Reset();
        ActualLapSO.Reset();
    }

    public void StopRecording()
    {
        Debug.Log("STOP RECORDING");
        shouldRecord = false;
        //Comprobamos tiempos

        
        bestLapSO = ActualLapSO;
        
    }
    #endregion

    #region PLAY GHOST DATA
    public void StartPlaying()
    {
        Debug.Log("START PLAYING");
        shouldPlay = true;
        shouldRecord = false;

        // Seteamos los valores iniciales
        totalPlayedTime = 0;
        currentSampleToPlay = 0;
        currenttimeBetweenPlaySamples = 0;

        // Desactivamos el control del coche
        carToPlay.GetComponent<CarController>().enabled = false;
        carToPlay.GetComponent<CarUserControl>().enabled = false;

    }

    public void StopPlaying()
    {
        Debug.Log("STOP PLAYING");
        shouldPlay = false;

        // Devolvemos el control al coche por si fuera necesario (opcional)
        carToPlay.GetComponent<CarController>().enabled = true;
        carToPlay.GetComponent<CarUserControl>().enabled = true;

    }
    #endregion
    

    private void Update()
    {
        HandleTestActionInputs();
        //Cambiamos la bandera de Record cuando se inicia la carrera
        if (carToRecord.GetComponent<CarController>().loops > 0 && carToRecord.GetComponent<CarController>().loops <= carToRecord.GetComponent<CarController>().maxLoops)
        {
            flagRecord = true;
        }
        else {

            flagRecord = false;

        }

        if (carToRecord.GetComponent<CarController>().loops > carToRecord.GetComponent<CarController>().maxLoops) {

            //StopRecording();
            StopPlaying();
            
        }

        if (shouldRecord || (flagRecord ))
        {
            Debug.Log("Grabando");
            // A cada frame incrementamos el tiempo transcurrido 
            totalRecordedTime += Time.deltaTime;
            currenttimeBetweenSamples += Time.deltaTime;

            // Si el tiempo transcurrido es mayor que el tiempo de muestreo
            if (currenttimeBetweenSamples >= timeBetweenSamples)
            {
                // Guardamos la informaci�n para el fantasma
                //bestLapSO.AddNewData(carToRecord.transform);
                ActualLapSO.AddNewData(carToRecord.transform);
                // Dejamos el tiempo extra entre una muestra y otra
                currenttimeBetweenSamples -= timeBetweenSamples;
            }
        }
        
        if (shouldPlay || (flagRecord))
        {
            Debug.Log("Playing");
            // A cada frame incrementamos el tiempo transcurrido 
            totalPlayedTime += Time.deltaTime;
            currenttimeBetweenPlaySamples += Time.deltaTime;

            // Si el tiempo transcurrido es mayor que el tiempo de muestreo
            if (currenttimeBetweenPlaySamples >= timeBetweenSamples)
            {
                // De cara a interpolar de una manera fluida la posici�n del coche entre una muestra y otra,
                // guardamos la posici�n y la rotaci�n de la anterior muestra
                lastSamplePosition = nextPosition;
                lastSampleRotation = nextRotation;

                // Cogemos los datos del scriptable object
                bestLapSO.GetDataAt(currentSampleToPlay, out nextPosition, out nextRotation);

                // Dejamos el tiempo extra entre una muestra y otra
                currenttimeBetweenPlaySamples -= timeBetweenSamples;

                // Incrementamos el contador de muestras
                currentSampleToPlay++;
            }

            // De cara a crear una interpolaci�n suave entre la posici�n y rotaci�n entre una muestra y la otra, 
            // calculamos a nivel de tiempo entre muestras el porcentaje en el que nos encontramos
            float percentageBetweenFrames = currenttimeBetweenPlaySamples / timeBetweenSamples;
            

            // Aplicamos un lerp entre las posiciones y rotaciones de la muestra anterior y la siguiente seg�n el procentaje actual.
            carToPlay.transform.position = Vector3.Slerp(lastSamplePosition, nextPosition, percentageBetweenFrames);
            carToPlay.transform.rotation = Quaternion.Slerp(lastSampleRotation, nextRotation, percentageBetweenFrames);
        }
        if (shouldRePlay)
        {
            Debug.Log("Playing");
            // A cada frame incrementamos el tiempo transcurrido 
            totalPlayedTime += Time.deltaTime;
            currenttimeBetweenPlaySamples += Time.deltaTime;

            // Si el tiempo transcurrido es mayor que el tiempo de muestreo
            if (currenttimeBetweenPlaySamples >= timeBetweenSamples)
            {
                // De cara a interpolar de una manera fluida la posici�n del coche entre una muestra y otra,
                // guardamos la posici�n y la rotaci�n de la anterior muestra
                lastSamplePosition = nextPosition;
                lastSampleRotation = nextRotation;

                // Cogemos los datos del scriptable object
                ActualLapSO.GetDataAt(currentSampleToPlay, out nextPosition, out nextRotation);

                // Dejamos el tiempo extra entre una muestra y otra
                currenttimeBetweenPlaySamples -= timeBetweenSamples;

                // Incrementamos el contador de muestras
                currentSampleToPlay++;
            }

            // De cara a crear una interpolaci�n suave entre la posici�n y rotaci�n entre una muestra y la otra, 
            // calculamos a nivel de tiempo entre muestras el porcentaje en el que nos encontramos
            float percentageBetweenFrames = currenttimeBetweenPlaySamples / timeBetweenSamples;


            // Aplicamos un lerp entre las posiciones y rotaciones de la muestra anterior y la siguiente seg�n el procentaje actual.
            carToPlay.transform.position = Vector3.Slerp(lastSamplePosition, nextPosition, percentageBetweenFrames);
            carToPlay.transform.rotation = Quaternion.Slerp(lastSampleRotation, nextRotation, percentageBetweenFrames);
        }
    }


    void HandleTestActionInputs()
    {
        // START/STOP RECORDING
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (shouldRecord)
                StopRecording();
            else
                StartRecording();
        }

        // PLAY RECORDED LAP
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (shouldPlay)
                StopPlaying();
            else
                StartPlaying();
        }

        // RESET
        if (Input.GetKeyDown(KeyCode.Delete))
            bestLapSO.Reset();
    }
    



}
