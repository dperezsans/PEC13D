using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalGameController : MonoBehaviour
{
    [Serializable]
    public struct DataSave
    {
        public int bestScoreMin;
        public int bestScoreSeg;
        public int bestLoopMin;
        public int bestLoopSeg;
        public int lastScoreMin;
        public int lastScoreSeg;

        public override string ToString() { return "BSM: " + bestScoreMin + ", BSS: " + bestScoreSeg + ", BLM: " + bestLoopMin + ", BLS: " + bestLoopSeg + ", LSM: " + lastScoreMin + ", LSS: " + lastScoreSeg; }
    }

    string fileName = Application.dataPath + "/XML/SavedData.xml";

    [SerializeField] TMP_Text bestScore;
    [SerializeField] TMP_Text fastLoop;
    [SerializeField] TMP_Text lastScore;


    private int BSM = 0;
    private int BSS = 0;
    private int BLM = 0;
    private int BLS = 0;
    private int LSM = 0;
    private int LSS = 0;

    // Start is called before the first frame update
    public void Awake()
    {
        Load();
        bestScore.text = "Best Score: "+string.Format("{00:00}:{01:00}", BSM, BSS);
        fastLoop.text = "Best Actual Loop: " + string.Format("{00:00}:{01:00}", BLM, BLS);
        lastScore.text = "Last Score: " + string.Format("{00:00}:{01:00}", LSM, LSS);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void NavegateTo()
    {
        SceneManager.LoadScene("Game");
    }

    public void Load()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(DataSave));

        using (FileStream stream = new FileStream(fileName, FileMode.Open))
        {
            DataSave dataSave = (DataSave)serializer.Deserialize(stream);

            BSM = dataSave.bestScoreMin;
            BSS = dataSave.bestScoreSeg;
            BLM = dataSave.bestLoopMin;
            BLS = dataSave.bestLoopSeg;
            LSM = dataSave.lastScoreMin;
            LSS = dataSave.lastScoreSeg;
        }
    }
}
