using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using TMPro;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Vehicles.Car;



public class UIController : MonoBehaviour
{
    [Serializable]
    public struct DataSave
    {
        public int bestScoreMin ;
        public int bestScoreSeg;
        public int bestLoopMin;
        public int bestLoopSeg;
        public int lastScoreMin;
        public int lastScoreSeg;
        public string carPositions;
        public string carRotations;


        public override string ToString() { return "BSM: " + bestScoreMin + ", BSS: " + bestScoreSeg + ", BLM: " + bestLoopMin + ", BLS: " + bestLoopSeg + ", LSM: " + lastScoreMin + ", LSS: " + lastScoreSeg+ ", positions: " + carPositions + ", rotations: " + carRotations; }
    }

    string fileName = Application.dataPath + "/XML/SavedData.xml";

    

    [SerializeField] int min, seg;
    [SerializeField] int minLoop, segLoop;
    [SerializeField] TMP_Text timer;
    [SerializeField] TMP_Text loops;
    [SerializeField] TMP_Text timeLoopOne;
    [SerializeField] TMP_Text timeLoopTwo;
    [SerializeField] TMP_Text timeLoopThree;
    [SerializeField] GameObject CarPlayer;
    [SerializeField] GameObject ghostManager;
    

    public Boolean activeChrono = false;
    private float realTime = 0.0f;
    private float realTimeLoop = 0.0f;
    private int actualLoop = 0;

    private int minLoopMin = 999;
    private int minLoopSeg = 999;

    private int BSM = 0;
    private int BSS = 0;

    //posicion inicial del coche
    Vector3 initPosition;
    Quaternion initRotation;

    public List<Vector3> bestPositions;
    public List<Quaternion> bestRotations;
    public int bestSample;
    public Boolean saveBest = false;
    public GhostLapData ActualLapSO;
    private Vector3 nextPosition;
    private Quaternion nextRotation;

    private Boolean shouldRedord = false;
    private Boolean recording = true;

    private void Awake()
    {
        min = 0;
        seg = 0;
        minLoop = 0;
        segLoop = 0;

        timeLoopOne.text = "- -";
        timeLoopTwo.text = "- -";
        timeLoopThree.text = "- -";

        actualLoop = CarPlayer.GetComponent<CarController>().loops;

        initPosition = CarPlayer.transform.position;
        initRotation = CarPlayer.transform.rotation;
        saveBest = true;
        Load();
        ghostManager.GetComponent<GhostManager>().bestLapSO.Reset();
        ghostManager.GetComponent<GhostManager>().bestLapSO.carPositions = bestPositions;
        ghostManager.GetComponent<GhostManager>().bestLapSO.carRotations = bestRotations;
        

    }
    // Update is called once per frame
    void Update()
    {
        
        if (CarPlayer.GetComponent<CarController>().loops >= 1 && CarPlayer.GetComponent<CarController>().loops <= 3)
        {
            activeChrono = true;
            
            //ghostManager.GetComponent<GhostManager>().shouldPlay = true ;
        }
        else {

            activeChrono = false;
            //ghostManager.GetComponent<GhostManager>().shouldPlay = false ;
            
        }



        if (activeChrono == true)
        {
            realTime += Time.deltaTime;
            realTimeLoop += Time.deltaTime;
            loops.text = "Loop: " + CarPlayer.GetComponent<CarController>().loops;

        }
        else {
            loops.text = "Loop: " + "- -";
        }
        
        if (recording == false && activeChrono == true) {
            shouldRedord = true;
           
            recording = true;
        }

        int tmpMin = Mathf.FloorToInt(realTime / 60);
        int tmpSeg = Mathf.FloorToInt(realTime % 60);
        min = tmpMin;
        seg = tmpSeg;
        timer.text = string.Format("{00:00}:{01:00}", tmpMin, tmpSeg);


        if (actualLoop != CarPlayer.GetComponent<CarController>().loops) {
            int tmpMinLoop = Mathf.FloorToInt(realTimeLoop / 60);
            int tmpSegLoop = Mathf.FloorToInt(realTimeLoop % 60);

            switch (actualLoop) {
                case 1:
                    timeLoopOne.text = string.Format("{00:00}:{01:00}", tmpMinLoop, tmpSegLoop);
                    // registro de la mejor vuelta
                    if (tmpMinLoop < minLoopMin)
                    {
                        minLoopMin = tmpMinLoop;
                        minLoopSeg = tmpSegLoop;
                    }
                    else if (tmpMinLoop == minLoopMin && tmpSegLoop < minLoopSeg)
                    {
                        minLoopSeg = tmpSegLoop;
                    }
                    break;
                case 2:
                    timeLoopTwo.text = string.Format("{00:00}:{01:00}", tmpMinLoop, tmpSegLoop);
                    // registro de la mejor vuelta
                    if (tmpMinLoop < minLoopMin)
                    {
                        minLoopMin = tmpMinLoop;
                        minLoopSeg = tmpSegLoop;
                    }
                    else if (tmpMinLoop == minLoopMin && tmpSegLoop < minLoopSeg)
                    {
                        minLoopSeg = tmpSegLoop;
                    }
                    break;
                case 3:
                    timeLoopThree.text = string.Format("{00:00}:{01:00}", tmpMinLoop, tmpSegLoop);
                    // registro de la mejor vuelta
                    if (tmpMinLoop < minLoopMin)
                    {
                        minLoopMin = tmpMinLoop;
                        minLoopSeg = tmpSegLoop;
                    }
                    else if (tmpMinLoop == minLoopMin && tmpSegLoop < minLoopSeg)
                    {
                        minLoopSeg = tmpSegLoop;
                    }
                    break;
                default:
                    break;
            }

            
            actualLoop = CarPlayer.GetComponent<CarController>().loops;
            realTimeLoop = 0.0f;
        }

        

        if (CarPlayer.GetComponent<CarController>().loops > 3) {
            if (BSM > min)
            {
                BSM = min;
                BSS = seg;
                saveBest = true;
                // ghostManager.GetComponent<GhostManager>().replace = true;
            }
            else if (BSM == min && BSS > seg)
            {

                BSS = seg;
                //ghostManager.GetComponent<GhostManager>().replace = true;
                saveBest = true;
            }
            else {
                saveBest = false;
            }
            CarPlayer.GetComponent<CarController>().loops = 0;
            actualLoop = 0;
            ghostManager.GetComponent<GhostManager>().StopRecording();
            if (saveBest)
            {
                ActualLapSO = ghostManager.GetComponent<GhostManager>().ActualLapSO;

                bestPositions = ActualLapSO.carPositions;
                bestRotations = ActualLapSO.carRotations;


            }
            

            Save();

            
          



        }
    }

    public void Save()
    {
        string aux_bestPosition = "";
        string aux_bestRotation = "";
        
        if (saveBest)
        {
            
            saveBest = false;
            for (int i = 0; i < bestPositions.Count; i++)
            {
                aux_bestPosition = aux_bestPosition + String.Format("{0}:{1}:{2}:", bestPositions.ElementAt(i).x, bestPositions.ElementAt(i).y, bestPositions.ElementAt(i).z);
            }


            for (int i = 0; i < bestRotations.Count; i++)
            {
                aux_bestRotation = aux_bestRotation + String.Format("{0}:{1}:{2}:{3}:", bestRotations.ElementAt(i).x, bestRotations.ElementAt(i).y, bestRotations.ElementAt(i).z, bestRotations.ElementAt(i).w);
            }
        }

        
        var dataSave = new DataSave()
        {
            bestScoreMin = BSM,
            bestScoreSeg = BSS,
            bestLoopMin = minLoopMin,
            bestLoopSeg = minLoopSeg,
            lastScoreMin = min,
            lastScoreSeg = seg,
            carPositions = aux_bestPosition,
            carRotations = aux_bestRotation
        };

        XmlSerializer serializer = new XmlSerializer(typeof(DataSave));

        using (FileStream stream = new FileStream(fileName, FileMode.Create))
        {
            serializer.Serialize(stream, dataSave);
        }
    }

    public void Load()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(DataSave));

        using (FileStream stream = new FileStream(fileName, FileMode.Open))
        {
           DataSave dataSave = (DataSave)serializer.Deserialize(stream);
            string[] aux_list = dataSave.carPositions.Split(':');
            int i = 0;
            bestPositions.Clear();
            bestRotations.Clear();
            float x = 0;
            float y = 0;
            float z = 0;
            float w = 0;
            foreach (string aux in aux_list) {
                
                
                //x
                if (i == 0)
                {
                    x = float.Parse(aux, System.Globalization.CultureInfo.GetCultureInfo("es-ES"));
                    
                    i++;
                }//y
                else if (i == 1)
                {
                    y = float.Parse(aux, System.Globalization.CultureInfo.GetCultureInfo("es-ES"));
                    
                    i++;
                    //j
                }
                else if (i == 2)
                {
                    z = float.Parse(aux, System.Globalization.CultureInfo.GetCultureInfo("es-ES"));
                    
                    Vector3 v_aux = new Vector3(x, y, z);
                    bestPositions.Add(v_aux);
                    i = 0;
                }
            }
            aux_list = dataSave.carRotations.Split(':');
            i = 0;
            foreach (string aux in aux_list)
            {

                
                //x
                if (i == 0)
                {
                    x = float.Parse(aux, System.Globalization.CultureInfo.GetCultureInfo("es-ES"));
                   
                    i++;
                }//y
                else if (i == 1)
                {
                    y = float.Parse(aux, System.Globalization.CultureInfo.GetCultureInfo("es-ES"));
                   
                    i++;
                    //j
                }
                else if (i == 2)
                {
                    z = float.Parse(aux, System.Globalization.CultureInfo.GetCultureInfo("es-ES"));
                    
                    i++;
                }//w
                else if (i == 3) {
                    Quaternion q_aux = new Quaternion(x, y, z, w);
                    w = float.Parse(aux, System.Globalization.CultureInfo.GetCultureInfo("es-ES"));
                    bestRotations.Add(q_aux);
                    i = 0;
                }



            }


            BSM = dataSave.bestScoreMin;
            BSS = dataSave.bestScoreSeg;
        }
    }
   
   

    public void PressResume()
    {
        SceneManager.LoadScene("FinalGame");
    }
    public void PressReset()
    {
        CarPlayer.transform.position = initPosition;
        CarPlayer.transform.rotation = initRotation;
        activeChrono = false;
        CarPlayer.GetComponent<CarController>().loops = 0;
        min = 0;
        seg = 0;
        minLoop = 0;
        segLoop = 0;

        timeLoopOne.text = "- -";
        timeLoopTwo.text = "- -";
        timeLoopThree.text = "- -";

        actualLoop = CarPlayer.GetComponent<CarController>().loops;

        realTime = 0;
        realTimeLoop = 0;

        minLoopMin = 999;
        minLoopSeg = 999;

        BSM = 0;
        BSS = 0;

        CarPlayer.GetComponent<CarController>().goalOneActive = true;
        CarPlayer.GetComponent<CarController>().goalTwoActive = true;
        CarPlayer.GetComponent<CarController>().goalThreeActive = true;
        CarPlayer.GetComponent<CarController>().goalFourActive = true;
        ghostManager.GetComponent<GhostManager>().StopRecording();
        ghostManager.GetComponent<GhostManager>().StopPlaying();
        saveBest = true;

    }





}
