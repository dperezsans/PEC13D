# PEC1 3D 

## Introducción

Se solicita la creación de un juego de carreras a contrareloj. Para ello, se ha utilizado la plantilla ofrecida por el profesorado.

## Terreno

El terreno está compuesto por una extensión de 400x400. Con mucho desnivel, creando la sensación de un entorno montañoso.

Respecto a los terrenos, se han utilizado los siguientes elementos:

### Paint Textures
Se han utilizado tres tipos de texturas, que son las siguientes:
- Floor
- Rocks
- Grass
- Snow

### Paint Trees

Referente a los tipos de árboles, a parte de los tres facilitados por la plantilla, se ha añadido un tipo de árbol córtado. 

### Details

Los detalles usados son los facilitados por la plantilla, siendo estos dos tipos de hierva y un tipo palma.

## Circuito

Para el desarrollo del circuito, se ha optado por utilizar el paquete EasyRoad3D Free. Además, todo el circuito está colocado en la capa road con la etiqueta Road.

### Goals

Uno de los puntos que se ha tenido que añadir para evitar errores de contado de vueltas, ha sidos la incorporación de Goals, un total de cuatro. Estos son elementos transparentes que controlan. mediante el uso de banderas (booleanos), si el coche ha pasado o no por ese punto y si ha pasado en el orden correcto. De esta forma, se puede solucionar errores como hacer marcha atrás en la meta para contabilidad vueltas o pasar por sitios prohibidos que no estén bien controlados.

### Obstáculos y señales

Para dar mejor aspecto y jugabilidad, se han icorporado elementos con collider dentro del juego, como puede ser; barreras, señales o banderas de meta.

![Elementos](obstaculos.PNG)

## Vehículos

Se han creado dos tipos de vehículos dentro del juego.

### Vehículo principal
 Este vehículo es el controlado por el jugador. Se ha usado el facilitado por la plantilla y se le ha añadido un conjunto de materiales para ofrecer un mejor aspecto visual.

![Vehiculo](vehiculo.PNG)

#### Modificaciones

El vehículo cuenta con un conjunto de modificaciones para alterar su comportamiento frente al terreno.

El primero de todo se ha añadido un elemento collider de tipo trigger en la parte inferior del vehículo. Con el fin de detectar el terreno pisado. Así, de esta forma, se podrá alterar las velocidades pico según si circula por carretera o por otro terreno.

Para efectuar este efecto, se ha añadido en el Script CarController.cs el siguiente código:

```C#
 void OnTriggerExit(Collider other)
 {
     if (other.tag == "road") {

         speedTerrain = 3;
         m_Topspeed = 80;
         speedFactor = 2.23693629f / 2;
         

     }
 }
 ```
Fijando, para todo terreno que no sea road, la velocidad de el terreno a 3 en vez de 10, el top de velocidad del coche a 80 en vez de 200 y el factor de velocidad se convierte a la mitad.

Por otro lado, en CarController.cs por controlar el número de vueltas del coche. Utilizando los goals del terreno.

### Vehículo fantasma

Este vehículo es una copia del vehículo controlado por el jugador. Pero, con ligeros cambios. No es un vehículo de control, es un vehículo para simular la mejor carrera.

![Fantasma](fantasma.PNG)


En primer lugar, se ha aplicado un material semitransparente.

Seguidamente, se ha desactivado los Scripts asociados al elemento. No se necesita el control de este.

Por último, los collider se han convertido en trigger. De esta forma se evitarán choques con el vehículo principal.

#### Modificaciones

Para el desarrollo de este elemento, se ha tenido que modificar los Scripts facilitados por el profesorado, concretamente GhostManager.cs. Con el fin de iniciar el record/playing del vehículo en el momento exacto que se inicia la carrera para el jugador.

Así mismo, se ha optado por crear dos objetos de tipo GhostLapData. Uno es para almacenar la mejor carrera y el otro para almacenar la carrera actual. Se debe recordar, que no siempre que se corre será la mejor carrera. Es posible, que existe ya una carrera almacenada que sea mejor.

## Cámaras

El uso de las cámaras puede ser muy tedioso y complicado. Por ese motivo, se ha optado por el uso del paquete de Cinemachine.

Con este paquete, se ha añadido una cámara FreeLook, ajustando las tres posibles vista para que siempre se vea el coche o vehículo controlado por el usuario.


## Guardado de datos

Uno de los puntos importante de los juegos es la persistencia de datos entre partida y partida. Por ese motivo, se ha procedido ha realizar un guardado, en xml, de algunos elementos principales del juego:

- Mejor tiempo de carrera
- Mejor tiempo de vuelta acual
- Tiempo de la última carrera
- Recorrido de la mejor vuelta

![Estructura de datos](datos.PNG)

Con el almacenaje de este último dato, desde el incio de la carrera. El jugador podrá competir contra sí mismo (vehículo fantasma). Un punto siginificativo del almacenaje de la rotación y la posición del coche, es que se ha optado por separar los elemento con : y después procesarlos mediante código.

## Escenas

El juego cuenta con tres escenas:

### Menú de juego

![Scene Menu](menu.PNG)

### Pista
Es importante recalcar que este circuito cuenta con dos atajos permitidos.

![Scene Stage](circuito.PNG)

#### UI

Mientras se realiza la carrera, el jugador contará con información dentro del juego que son:
- Cronómetro
- Número de vueltas dadas
- Tiempo realizado en cada vuelta.

Por otro lado, se facilitan dos botones con los que se podrá reiniciar la carrera o se podrá finalizar la carrera e ir al resumen del juego.

![UX](ui.PNG)


### Resumen Final

![Scene Final](final.PNG)


## GamePlay

[Gameplay](https://youtu.be/aWeeEj8Fcv8)


